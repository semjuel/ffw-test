jQuery(document).ready(function($) {
  $(".slider").royalSlider({
    imageScaleMode: 'fit-if-smaller',
    controlNavigation: 'bullets',
    arrowsNav: 'true',
    /*arrowsNavAutoHide: false,*/
    loop: 'true',
    keyboardNavEnabled: true
  });

  if ($(window).width() > 480) {
    $('.w-slider-wrapper').slick({
      centerMode: false,
      slidesToShow: 3,
      slidesToScroll: 3
    });

    $('.prev-nav').click(function(){
      $('.slick-prev').click();
      return false;
    });
    $('.next-nav').click(function(){
      $('.slick-next').click();
      return false;
    });
  }

  if ($(window).width() == 480) {
    $('.mobile-top-menu').click(function(){
      $('.bottom-row .main-menu').toggleClass('show-menu');
    });
  }
});