Small description of what I've made:
1. Created folder 'mysearch' and put in 'sites/all/modules' folder in Drupal.
2. Renamed past file 'mysearch.module.php' file into 'mysearch' folder (see 1) and renamed this file into 'mysearch.module'.
3. Created file 'mysearch.info' in 'mysearch' folder.
4. Added some information in 'mysearch.info' file (see file).
5. Rewrote 'mysearch_searchpage()' totally:
- created custom template file – mysearch.tpl.php. Because it's a bad idea to write html tags within the functions.
- $get_node_result – undefined variable
- $query = "
   SELECT nid
   FROM node_revisions
   WHERE body LIKE '%$searchterm%'
   ";
What do you expect to find in 'node_revision' (stored information about each saved version of a node)
and there is no field 'body' in 'node_revision' table.

6. Overrode the query to database – now my db_select search for nodes which have word in 'title' or in 'body' – disadvantages
 of my query in that if we want find string  'some text' and in database will be a node with such body 'some <b>text<b/>'
 my select will not found this string.
 It's better to create new table with fields: 'id', 'node id', 'text' – and create function which will put to this
 table in field 'node id', in field 'text' node body without html tags and node title'. Also it’s better to make 'text' field INDEX.
 I didn't do this, because it was a test task and I don’t have much time now. But I’m capable to do it if it is necessary.
